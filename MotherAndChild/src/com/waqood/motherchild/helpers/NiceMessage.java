package com.waqood.motherchild.helpers;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class NiceMessage {

	private static	ProgressBar bar;
	static Context context;
	private static TextView niceMsg; 
	public static void initProgress(Context con, RelativeLayout main) {
		  bar  = new ProgressBar(con);

		  context = con;
		 RelativeLayout.LayoutParams labelLayoutParamsBoth = new RelativeLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			labelLayoutParamsBoth.addRule(RelativeLayout.CENTER_IN_PARENT,
					RelativeLayout.TRUE);
			bar.setLayoutParams(labelLayoutParamsBoth);

			bar.setVisibility(View.INVISIBLE);
			main.addView(bar);
	}
	
	public static void initNiceMessage(Context con, RelativeLayout main
			, int textColor, int bgColor)
	{
		
		RelativeLayout.LayoutParams rl_main_params = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

		main.setLayoutParams(rl_main_params);
	//	main.setPadding(0, 0, 0, 0);
		
		 niceMsg = new TextView(con);
		 niceMsg.setId(777);

		RelativeLayout.LayoutParams labelLayoutParamsText = new RelativeLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		labelLayoutParamsText.addRule(RelativeLayout.CENTER_HORIZONTAL );
		labelLayoutParamsText.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		niceMsg.setLayoutParams(labelLayoutParamsText);

		niceMsg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		niceMsg.setText("");
		Activity activity = (Activity) con;
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		float density = metrics.density;
		niceMsg.setPadding((int) (15 * density), (int) (15 * density),
				(int) (15 * density), (int) (15 * density));


		niceMsg.setTextColor(textColor);
		niceMsg.setGravity(Gravity.CENTER_HORIZONTAL );
		niceMsg.setBackgroundColor(bgColor);
		
		niceMsg.setVisibility(View.GONE);
		((ViewGroup) main).addView(niceMsg);


		
	}
	
	public static void initProgressTop(Context con, RelativeLayout main) {
		  bar  = new ProgressBar(con);

		  context = con;
		 RelativeLayout.LayoutParams labelLayoutParamsBoth = new RelativeLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			labelLayoutParamsBoth.addRule(RelativeLayout.ALIGN_PARENT_TOP,
					RelativeLayout.TRUE);
			labelLayoutParamsBoth.addRule(RelativeLayout.CENTER_HORIZONTAL,
					RelativeLayout.TRUE);
			bar.setLayoutParams(labelLayoutParamsBoth);

			bar.setVisibility(View.INVISIBLE);
			main.addView(bar);
	}
	
	public static void showBar() {

		bar.setVisibility(View.VISIBLE);
	}
	
	public static void hideBar() {

		bar.setVisibility(View.INVISIBLE);
	}
	
	
	public static void showMsg(String msg) {
		niceMsg.setText(msg);

		niceMsg.setVisibility(View.VISIBLE);
	}
	
	public static void hideMsg() {

		niceMsg.setVisibility(View.GONE);
	}
}