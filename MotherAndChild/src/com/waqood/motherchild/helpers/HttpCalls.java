package com.waqood.motherchild.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.loopj.android.http.*;
import com.waqood.motherandchild.Constants;

public class HttpCalls {
  private static final String BASE_URL = Constants.BASE_URL;

  private static AsyncHttpClient client = new AsyncHttpClient();

  public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
      client.get(getAbsoluteUrl(url), params, responseHandler);
  }

  public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
      client.post(getAbsoluteUrl(url), params, responseHandler);
      
  }

  private static String getAbsoluteUrl(String relativeUrl) {
	  Log.d("HTTP", BASE_URL + relativeUrl + "");
      return BASE_URL + relativeUrl;
  }
  
  public static boolean isNetworkAvailable(Context con) {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
  
  public static void handleFail(Context con)
  {

		if(HttpCalls.isNetworkAvailable(con))
			NiceMessage.showMsg(Constants.CONNECTION_FAILURE);
		else
			NiceMessage.showMsg(Constants.NO_CONNECTION);

  }
  }
