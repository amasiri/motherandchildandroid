package com.waqood.motherchild.push;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.waqood.motherandchild.Constants;
import com.waqood.motherandchild.MainScreens;
import com.waqood.motherandchild.R;
import com.waqood.motherandchild.SdCalls;
import com.waqood.motherchild.helpers.HttpCalls;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class PushRegisterService extends Service  {
	
	private NotificationManager mNotificationManager;
    public static final int NOTIFICATION_ID = 2;

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String PREF_GCM = "PREF_GCM";

    private static final String DEVICE_TOKEN = "device_token";
    private static final String DEVICE_TYPE= "device_type";

    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    String SENDER_ID = Constants.PUSH_SENDER_ID;


    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();

    private String regid;

	private static final String TAG = "PushRegisterService";

	@Override
	public void onCreate() 
	{
		Log.d(TAG, "onCreate");
	}




	AsyncHttpResponseHandler httpResponse = new AsyncHttpResponseHandler() {
		@Override
		public void onSuccess(String response) {
			Log.d(TAG, "response:" + response);

			stopSelf();

		}

		@Override
		public void onFailure(Throwable e, String str) {
			//sendRegistrationIdToBackend(ID);

			super.onFailure(e, str);
			e.printStackTrace();
		}

	};


	@Override
	public void onDestroy() {
		Log.d(TAG, "onDestroy");
	}

	@Override
	public void onStart(Intent intent, int startid) {

		Log.d(TAG, "onStart");

		if(intent != null)
		{	
			Log.d(TAG, "intent != null");

			if(checkPlayServices())
			{
				Bundle extras = intent.getExtras();
				

				if(extras.getBoolean(Constants.PUSH_STATUS)) // turn push on
				{
					gcm = GoogleCloudMessaging.getInstance(this);
					regid = getRegistrationId(this);
			
					if (regid.isEmpty()) 
					{
			
						registerInBackground();
					}
					else
					{
						sendRegistrationIdToBackend(regid);
	                    Log.d(TAG, " EXISTING ID  " + regid);

					}
				}
				else // turn push off
				{
					gcm = GoogleCloudMessaging.getInstance(this);
					regid = getRegistrationId(this);
					removeRegistrationIdToBackend();
				}
				
			}
			else
			{	
				stopSelf();
			}
		}
		else
		{
			Log.d(TAG, "intent is null");

		}

	}    




	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGcmPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(PushRegisterService.this);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    Log.d(TAG, " NEW ID  " + msg);
                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    sendRegistrationIdToBackend(regid);

                    // For this demo: we don't need to send it because the device will send
                    // upstream messages to a server that echo back the message using the
                    // 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(PushRegisterService.this, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {

            }
        }.execute(null, null, null);
    }

   

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGcmPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(PREF_GCM,
                Context.MODE_PRIVATE);
    }
    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend(String id) {
  		RequestParams httpParams = new RequestParams();
		httpParams.put("password",SdCalls.getPasswrod(this) );
		httpParams.put("username",SdCalls.getUseremail(this).replace("@", "." ));
		httpParams.put(DEVICE_TOKEN,id );
		httpParams.put(DEVICE_TYPE, "android");
		HttpCalls.get(Constants.LOGIN_URL, httpParams, httpResponse);
		
		
  		RequestParams httpParams2 = new RequestParams();
  		httpParams2.put("channels_id","1");
  		httpParams2.put(DEVICE_TOKEN,id );
  		httpParams2.put(DEVICE_TYPE, "android");
		HttpCalls.get(Constants.UPDATE_CHANNEL_URL, httpParams2, httpResponse);
    }
 
    
    private void removeRegistrationIdToBackend() {
  		RequestParams httpParams2 = new RequestParams();
  		httpParams2.put("channels_id","3");
  		httpParams2.put(DEVICE_TOKEN,regid );
  		httpParams2.put(DEVICE_TYPE, "android");
		HttpCalls.get(Constants.UPDATE_CHANNEL_URL, httpParams2, httpResponse);
    }
    
	 /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
            	sendNotification("Update Google Play Services", "You won't receive any notifications until you update Google Play Services");
            	/*
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();*/
            } else {
            	sendNotification("Device not supported", " Your device is not supported to receive notifications from us");
            	            }
            return false;
        }
        return true;
    }
    
    private void sendNotification(String msg, String title) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, MainScreens.class), 0);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(title)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
				.setAutoCancel(true)
				.setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
    
    
}
