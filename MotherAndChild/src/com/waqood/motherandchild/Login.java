package com.waqood.motherandchild;


import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.waqood.motherchild.helpers.HttpCalls;
import com.waqood.motherchild.helpers.Modules;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.view.MenuItem;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class Login extends Activity {

	public static final String RESPOND = "respond";
	public static final String MESSAGE = "message";

	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for email and password at the time of the login attempt.
	private String mEmail;
	private String mPassword;

	// UI references.
	private EditText mEmailView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private TextView mLoginStatusMessageView;
	private boolean mGetDate = false; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		setContentView(R.layout.login_layout);
		setupActionBar();

		// Set up the login form.
		//mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
		mEmailView = (EditText) findViewById(R.id.email);
		//mEmailView.setText(mEmail);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});

		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);

		findViewById(R.id.sign_in_button).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						attemptLogin();
					}
				});
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			// Show the Up button in the action bar.
			getActionBar().setDisplayHomeAsUpEnabled(true);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
				getActionBar().setHomeAsUpIndicator(R.drawable.ic_home_up);
			}
			
			

		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent n = new Intent(this,MainLogin.class);
			startActivity(n);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.login, menu);
		return false;
	}

	
	public void resetClicked(View view) {
		
		Intent n = new Intent(this,Reset.class);
		startActivity(n);
	}
	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		InputMethodManager	  imm = (InputMethodManager)this.getSystemService(Service.INPUT_METHOD_SERVICE);

		if(imm != null){
	        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	    }
		if (mAuthTask != null) {
			return;
		}

		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mEmail = mEmailView.getText().toString();
		mPassword = mPasswordView.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 4) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(mEmail)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!mEmail.contains("@")) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	JsonHttpResponseHandler httpResponse = new JsonHttpResponseHandler() {
		
	@Override
	public void onFailure(Throwable e, JSONObject json) {

		mAuthTask = null;
		showProgress(false);

		super.onFailure(e, json);
		e.printStackTrace();
	}
	
	@Override
	public void onRetry() {
		
		Log.d(this.getClass().getSimpleName(), " retry " );

	};
	public void onSuccess(JSONObject response) {
		Log.d(this.getClass().getSimpleName(), " response " + response.toString());

		mAuthTask = null;
		showProgress(false);

		try {
			if(response.getInt(RESPOND) == 0)
			{
				Toast.makeText(Login.this, response.getString(MESSAGE), Toast.LENGTH_SHORT).show();
			}
			else if(response.getInt(RESPOND) == 1)
			{

				if(mGetDate==false)
				{
					mGetDate=true;
					getDate();
				}
				else
				{
					Log.d(this.getClass().getSimpleName(), " response " + response.toString());
					
					String date = null; 
					try{
			            JSONArray posts = response.getJSONArray("result");
			            for (int i = 0; i < posts.length(); i++) {
			                JSONObject c = posts.getJSONObject(i);
			                 date = c.getString("ddate");
			            
			            }
					}
						catch (Exception e) 
						{
							e.printStackTrace();
						}
					moveToApp(date);
				}

			}
		} 
		catch (JSONException e) 
		{
			e.printStackTrace();
		}
	}

	};


	private void getDate() {
		
		showProgress(true);
		/*String url = "username="+mEmail.replace("@", ".");	
		HttpCalls.get(Constants.DUE_DATE_URL + url, null, httpResponse);
		*/
		
		RequestParams httpParams = new RequestParams();
		httpParams.put("username", mEmail.replace("@", "."));
		httpParams.put("service", Constants.GET_DUE_DATE_POST_PARAM);
		HttpCalls.post(Constants.DUE_DATE_POST_URL , httpParams, httpResponse);

	};
	
	
	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		@Override
		protected Boolean doInBackground(Void... params) {

			RequestParams httpParams = new RequestParams();
			httpParams.put("username", mEmail.replace("@", "."));
			httpParams.put("password", mPassword);
//			HttpCalls.get(Constants.LOGIN_URL, httpParams, httpResponse);
			HttpCalls.post(Constants.LOGIN_URL, httpParams, httpResponse);

			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}
	
	@Override       
	public void onBackPressed() {
		
		Intent n = new Intent(this,MainLogin.class);
		startActivity(n);
		finish();

		
		super.onBackPressed();
		
		
	};
	
	private void moveToApp(String date) {

		
		
        SdCalls.setOldWeek(Login.this, SdCalls.getCurrentWeek(Login.this));
		SdCalls.setUser(Login.this, mEmail, Modules.getYear(date), Modules.getMonth(date), Modules.getDay(date), mPassword);
		SdCalls.setNoti(Login.this, true);

		Intent n = new Intent(Login.this,MainScreens.class);
		n.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
		n.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(n);
		finish();
	}
	

}
