package com.waqood.motherandchild.alarm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.waqood.motherandchild.MainScreens;
import com.waqood.motherandchild.R;
import com.waqood.motherandchild.SdCalls;

/**
 * This {@code IntentService} does the app's actual work.
 * {@code SampleAlarmReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class SampleSchedulingService extends IntentService {
	public SampleSchedulingService() {
		super("SchedulingService");
	}

	// An ID used to post the notification.
	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	@Override
	protected void onHandleIntent(Intent intent) {

		if (SdCalls.isNewWeekYet(this)) {
			Log.d(this.getClass().getSimpleName(), "Yes new week" + "");

			String notyMsg = "مبروك, لقد دخلتي أسبوعا جديدا";
			sendNotification("أسبوع جديد", notyMsg);
		} else {
			Log.d(this.getClass().getSimpleName(), "SAME week :( " + "");

		}
		// Release the wake lock provided by the BroadcastReceiver.
		SampleAlarmReceiver.completeWakefulIntent(intent);
	}

	// Post a notification indicating whether a doodle was found.
	private void sendNotification(String title, String msg) {
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
				new Intent(this, MainScreens.class), 0);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(title)
				.setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
				.setDefaults(Notification.DEFAULT_ALL)
				.setAutoCancel(true).setContentText(msg);
		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}

}
