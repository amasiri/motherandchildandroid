package com.waqood.motherandchild;

import org.joda.time.DateTime;
import org.joda.time.Weeks;

import android.content.Context;
import android.content.SharedPreferences;

public class SdCalls {

	/**
	 * Name of SD file
	 */
	public static final String PREFS_NAME = "MotherChildPref";
	public static final String first = "first";
	public static final String email = "email";
	public static final String mYear = "year";
	public static final String mMonth = "month";
	public static final String mDay = "day";
	public static final String mNotiGlobal = "Globalnoti";
	public static final String mNotiLocal = "Localnoti";
	public static final String mPassword = "password";
	public static final String mOldWeek = "oldWeek";
	public static final String mCurrentWeek = "currentWeek";
	public static final String TAG = "SdCalls";
    public static final int NOTIFICATION_ID = 1;

    
    public static void reset(Context con)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = uPreferences.edit();
		editor.clear();
		editor.commit();
    }
    
    public static boolean isFirst(Context con)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		boolean result = uPreferences.getBoolean(first, true);
		return result;   
    }
    
    public static void setUser (Context con, String useremail, int year, int month,
    		int day, String password)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = uPreferences.edit();
		editor.putBoolean(first, false);
		editor.putString(email, useremail);
		editor.putString(mPassword, password);
		editor.putInt(mYear, year);
		editor.putInt(mMonth, month);
		editor.putInt(mDay, day);
		editor.commit();
    }
    
    public static int getMonth(Context con)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		int month = uPreferences.getInt(mMonth,1);
		return month;
    }

    public static int getYear(Context con)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		int year = uPreferences.getInt(mYear,2014);
		return year;
    }

    
    public static int getday(Context con)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		int day = uPreferences.getInt(mDay,1);
		return day;
    }

    public static void setDate (Context con,  int year, int month,
    		int day)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = uPreferences.edit();
		editor.putInt(mYear, year);
		editor.putInt(mMonth, month);
		editor.putInt(mDay, day);
		editor.commit();
    }
    

    public static void setNoti (Context con, boolean pref)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = uPreferences.edit();
		editor.putBoolean(mNotiGlobal, pref);
		editor.commit();
    }
    
    
    public static boolean getNoti(Context con)
    {
    	
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		boolean result = uPreferences.getBoolean(mNotiGlobal, true);
		return result;   
    }
    
    
    public static void setNotiLocal (Context con, boolean pref)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = uPreferences.edit();
		editor.putBoolean(mNotiLocal, pref);
		editor.commit();
    }
    
    
    public static boolean getNotiLocal(Context con)
    {
    	
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		boolean result = uPreferences.getBoolean(mNotiLocal, true);
		return result;   
    }
    
    public static String getUseremail(Context con)
    {
    	
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		String result = uPreferences.getString(email, "");
		return result;   
    }

    public static String getPasswrod(Context con)
    {
    	
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		String result = uPreferences.getString(mPassword, "");
		return result;   
    }
    public static int getCurrentWeek(Context con)
    {
    	int week=0;
		int MAX_WEEKS = Constants.MAX_WEEKS; 

		DateTime today = new DateTime();
		DateTime expected = new DateTime(getYear(con), getMonth(con), getday(con), 00,00);

		int weeksDiff = Weeks.weeksBetween(today, expected).getWeeks();

		week = MAX_WEEKS - weeksDiff;
    	
		if(week <3)
			week=-1;
		
    	return week;
    }
    
    
    public static void setCurrentWeek (Context con, int week)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = uPreferences.edit();
		editor.putInt(mCurrentWeek, week);
		editor.commit();
    }

    public static void setOldWeek (Context con, int week)
    {
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = uPreferences.edit();
		editor.putInt(mOldWeek, week);
		editor.commit();
    }
    
    
    public static int getOldWeek(Context con)
    {
    	
    	SharedPreferences uPreferences = con.getSharedPreferences(PREFS_NAME, 0);
		int result = uPreferences.getInt(mOldWeek, -1);
		return result;   
    }
    
    
    public static boolean isNewWeekYet(Context con)
    {
    
       	int currentWeek = getCurrentWeek(con);
    	int oldWeek = getOldWeek(con);
    	
    	if(oldWeek<0)
    	{
    		setOldWeek(con, currentWeek);
    		return false;
    	}
    	else if ( oldWeek == currentWeek)
    	{
    		// do nothing
    		return false; 
    	}
    	else 
    	{
    		setOldWeek(con, currentWeek);
    		return true;
    	}
    	
    }
    

}

