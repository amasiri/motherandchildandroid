package com.waqood.motherandchild;




import com.waqood.motherchild.push.PushRegisterService;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class MainScreens extends FragmentActivity implements
ActionBar.TabListener {

	private SectionsPagerAdapter mSectionsPagerAdapter;

    
    
    public interface Callbacks {
        public void onBackPressedCallback();
    }
	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	TextView round1;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setProgressBarIndeterminate(true);
		setContentView(R.layout.main_screens_layout);

		if(SdCalls.getNoti(this))
			setupPush(true);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		
		
		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		
		}

	
	}

	private void setupPush(boolean status) {

		Intent i = new Intent(this, PushRegisterService.class);	 
		i.putExtra(Constants.PUSH_STATUS, status);

		startService(i);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

Log.d(this.getClass().getSimpleName(), " onCreateOptionsMenu" + "");
		getMenuInflater().inflate(R.menu.main_screens, menu);
		return true;
	}
	 @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
		 startActivityForResult(new Intent(this,Settings.class), 100);
		 return true;
	    }
	 

	
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			
			switch(position){
			case 0:
				Fragment fragment = new CalanderFragment();
				return fragment;
			case 1:
				Fragment fragment1 = new VideosFragment();

				return fragment1;
			case 2:
				Fragment fragment2 = new ArticlesFragment();
				return fragment2;
			}
			return null;
			
		}
		
		public void settingsOpen(View v){
			
		//	startActivity(new Intent(MainActivity.this,SettingsView.class));
		}

		@Override
		public int getCount() {
			return Constants.TabsCount;
		}
		
		

		@Override
		public CharSequence getPageTitle(int position) {
			
			switch (position) {
			case 0:
				return getString(R.string.CalenderTab);
			case 1:
				return getString(R.string.VideosTab);
			case 2:
				return getString(R.string.ArticlesTab);

			}
			return null;
		}
	}



	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mViewPager.setCurrentItem(tab.getPosition());
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		
	}

    public void showBar() {
	 	setProgressBarIndeterminateVisibility(true);




}
 public void hideBar() {
	 	setProgressBarIndeterminateVisibility(false);

}
 
 @Override
public void onBackPressed() {

	 
	 
/*	 if(mSectionsPagerAdapter.getItem(getActionBar().getSelectedNavigationIndex()) instanceof VideosFragment)
	 {
		 sendBackPressed();		 
	 }
	 
	 else
	 {
		 super.onBackPressed();
	 }
*/	 
	 
	 super.onBackPressed();

 }
	
	 @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	 {
		 
		 Log.d(this.getClass().getSimpleName(), "main " + requestCode + " " + resultCode);;

		 if(requestCode ==100)
		 {
			 if (resultCode == 111 ) 
					finish();
					
		 }

		 
		 
		super.onActivityResult( requestCode,  resultCode,  data) ;	
	 }

	 @Override
	protected void onResume() {

			Intent intent = getIntent();    
			if(intent !=null && intent.getExtras() != null)
			{
				int index =2;
				try{
			index = intent.getExtras().getInt("index");
			getActionBar().setSelectedNavigationItem(index);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}

	super.onResume();
	}
	 

}
