package com.waqood.motherandchild;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.waqood.motherandchild.adapters.ArticlesAdapter;
import com.waqood.motherchild.helpers.HttpCalls;
import com.waqood.motherchild.helpers.NiceMessage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.loopj.android.http.*;

public class ArticlesFragment extends Fragment implements OnItemClickListener, OnLastItemVisibleListener{
	private PullToRefreshListView list;
	private ArticlesAdapter adapter;
	ArrayList<HashMap<String, String>> listContent = new ArrayList<HashMap<String, String>>();
	private AdView mAdView;
	private AsyncHttpClient client;

	private int pageIndex=0;
	private RelativeLayout rl_main;

	  // JSON Node names
    private static final String TAG_POSTS = "posts";
    private static final String TAG_ID = "ID";
    private static final String TAG_DATE = "date";
    private static final String TAG_TITLE = "title";
    private static final String TAG_CONTENT = "content";
    private static final String TAG_IMAGE = "featured_image";
  	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		  
    	Log.d(this.getClass().getSimpleName(), " onCreateView " + "");

  		 client = new AsyncHttpClient();
		View rootView = inflater.inflate(R.layout.article_fragment_layout,
				container, false);
		rl_main = (RelativeLayout) rootView.findViewById(R.id.RL_MAIN);

		int bgColor = getActivity().getResources().getColor(R.color.green);
		int textColor = getActivity().getResources().getColor(R.color.white);

		NiceMessage.initNiceMessage(getActivity(), rl_main,  textColor, bgColor);
		
		list = (PullToRefreshListView) rootView.findViewById(R.id.LIST);
        mAdView = (AdView) rootView.findViewById(R.id.adView);
        mAdView.loadAd(new AdRequest.Builder().build());
        

		adapter = new ArticlesAdapter(getActivity(), listContent);
	
		list.setOnLastItemVisibleListener(this);
		list.setAdapter(adapter);	

		
		list.setOnItemClickListener(this);

		   list.setOnRefreshListener(new OnRefreshListener<ListView>() {
				@Override
				public void onRefresh(PullToRefreshBase<ListView> refreshView) {
			
				getPosts();
				}
			});
		   
		   
	        getPosts();


		return rootView;
	}
	

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		
		position--;
		Intent n = new Intent(getActivity(),ArticleDetails.class);
		n.putExtra("title", listContent.get(position).get("title"));
		n.putExtra("content", listContent.get(position).get("contentLong"));
		n.putExtra("image", listContent.get(position).get("image"));
		n.putExtra("date", listContent.get(position).get("date"));
		n.putExtra("id", listContent.get(position).get("id"));
		startActivity(n);

	}
	
    @Override
    public void onPause() {
        mAdView.pause();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdView.resume();
        
        
    }

    private void getPosts() {

    	Log.d(this.getClass().getSimpleName(), " GETTING POSTS" + "");
        pageIndex=0;

    	showBar();
    	String url = "http://public-api.wordpress.com/rest/v1/sites/motherchildguide.com/posts/?page="
    	+String.valueOf(pageIndex)+"&pretty=1";
        client.get(url, null, httpResponse);
        Log.d(this.getClass().getSimpleName(), "timeout  " +  client.getTimeout());
        
        
   
	}


	@Override
	public void onDestroy() {
        mAdView.destroy();
        super.onDestroy();
    }
	
	JsonHttpResponseHandler httpResponse = new JsonHttpResponseHandler() {
			@Override
		public void onFailure(Throwable e, JSONObject json) {

			
			HttpCalls.handleFail(getActivity());
			hideBar();
			list.onRefreshComplete();
	        
			super.onFailure(e, json);
			e.printStackTrace();
		}
		
		public void onSuccess(JSONObject response) {

			NiceMessage.hideMsg();
			if(pageIndex<1)
				listContent.clear();
		
			try{
            JSONArray posts = response.getJSONArray(TAG_POSTS);
            for (int i = 0; i < posts.length(); i++) {
                JSONObject c = posts.getJSONObject(i);
                 
                String id = c.getString(TAG_ID);
                String title = c.getString(TAG_TITLE);
                String content = c.getString(TAG_CONTENT);
                String image = c.getString(TAG_IMAGE);
                String date = c.getString(TAG_DATE);

				if(content.length()>5)
				{
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("title", title);
				map.put("date", date);
				map.put("contentLong", content);
				if(content.length()>300)
					content = content.substring(0,300) + " ... ";            
				map.put("content", content);
				map.put("image", image);
				map.put("id", id);
			
					listContent.add(map);	
				}

            }

			if(pageIndex<1)
			{
				adapter = new ArticlesAdapter(getActivity(), listContent);
				list.setAdapter(adapter);
				list.onRefreshComplete();
	            adapter.notifyDataSetChanged();
                list.setVisibility(View.VISIBLE);
			}
			else
			{

            adapter.notifyDataSetChanged();
			list.onRefreshComplete();
			
			}
            hideBar();

			}
			catch (Exception e) {
				e.printStackTrace();

			}
		

			
			
		};

	};
	
	
	 private void showBar() {
			if (getActivity() == null)
				return;
			MainScreens ra = (MainScreens) getActivity();
			ra.showBar();
	}
	 private void hideBar() {
			if (getActivity() == null)
				return;
			MainScreens ra = (MainScreens) getActivity();
			ra.hideBar();
	
	 }


	@Override
	public void onLastItemVisible() {	
		getMorePosts();
	}


	private void getMorePosts() {
        pageIndex++;
		showBar();
    	String url = "https://public-api.wordpress.com/rest/v1/sites/motherchildguide.com/posts/?page="
    	+String.valueOf(pageIndex)+"&pretty=1";
        client.get(url, null, httpResponse);
	}
	

}
