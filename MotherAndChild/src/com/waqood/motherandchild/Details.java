package com.waqood.motherandchild;


import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

public class Details extends Activity {

	private WebView webv;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.details_layout);
		
		TextView title = (TextView) findViewById(R.id.DETAILS_TITLE);
		ImageView image = (ImageView) findViewById(R.id.DETAILS_IMAGE);

	      Intent intent = getIntent();
	        String titleStr = intent.getExtras().getString("title");
	        int index = intent.getExtras().getInt("index");
	        String indexStr = String.valueOf(index+Constants.WEEKS_OFFSET);

	        title.setText(titleStr);

	        
	        String imageName = "week_" + indexStr ;//+ ".png";

			int id = getResources().getIdentifier(imageName, "drawable", getPackageName());
			image.setImageResource(id);

	        
	        String htmlFileTitle = "file:///android_asset/week_" + indexStr + ".html";
	        webv = (WebView)this.findViewById(R.id.DETAILS_WEB);
	        webv.loadUrl(htmlFileTitle); 
	        webv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
	        webv.setBackgroundColor(0x00000000);
	        if (Build.VERSION.SDK_INT >= 11) webv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
	        webv.setWebViewClient(new WebViewClient()
	        {
	            @Override
	            public void onPageFinished(WebView view, String url)
	            {
	            	webv.setBackgroundColor(0x00000000);
	                if (Build.VERSION.SDK_INT >= 11) webv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
	            }
	        });
	  	        
		setupActionBar();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.details, menu);
		return false;
	}
	
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
				getActionBar().setHomeAsUpIndicator(R.drawable.ic_home_up);
			}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			finish();
		//	finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
