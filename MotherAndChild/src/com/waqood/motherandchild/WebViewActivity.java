package com.waqood.motherandchild;

import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.support.v4.app.NavUtils;

public class WebViewActivity extends Activity {

	private int mType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setProgressBarIndeterminate(true);
		setContentView(R.layout.activity_web_view);
		
		Intent intent = getIntent();    
		mType = intent.getExtras().getInt("type");
	
		// Show the Up button in the action bar.
		setupActionBar(mType);
		
		WebView   webview = (WebView)findViewById(R.id.WEB_VIEW_CALC);

		
		 // Let's display the progress in the activity title bar, like the
		 // browser app does.
//		 getWindow().requestFeature(Window.FEATURE_PROGRESS);

		 webview.getSettings().setJavaScriptEnabled(true);

/*		 final Activity activity = this;
		 webview.setWebChromeClient(new WebChromeClient() {
		   public void onProgressChanged(WebViewActivity view, int progress) {
		     // Activities and WebViews measure progress with different scales.
		     // The progress meter will automatically disappear when we reach 100%
		     activity.setProgress(progress * 1000);
		   }
		 });
		 webview.setWebViewClient(new WebViewClient() {
		   public void onReceivedError(WebViewActivity view, int errorCode, String description, String failingUrl) {
		     Toast.makeText(activity, "خطأ " + description, Toast.LENGTH_SHORT).show();
		   }
		 });
*/
		 
		 webview.setWebViewClient(new WebViewClient() {

			   public void onPageFinished(WebView view, String url) {

				   hideBar();

			   }
			});
		 
		 showBar();
			if(mType==1)
		 webview.loadUrl("http://motherchildguide.com/api/pregnancy/android.php");
			else
				 webview.loadUrl("http://motherchildguide.com/api/ovulation/android.php");


	}


	private void setupActionBar(int type) {

		getActionBar().setDisplayHomeAsUpEnabled(true);
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
				getActionBar().setHomeAsUpIndicator(R.drawable.ic_home_up);
			}
		if(type==1)
		getActionBar().setTitle("حاسبة الحمل");
		else
			getActionBar().setTitle("حاسبة الاباضة");


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	//	getMenuInflater().inflate(R.menu.web_view, menu);
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
    public void showBar() {
	 	setProgressBarIndeterminateVisibility(true);




}
 public void hideBar() {
	 	setProgressBarIndeterminateVisibility(false);

}

}
