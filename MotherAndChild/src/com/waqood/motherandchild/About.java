package com.waqood.motherandchild;


import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;

public class About extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_layout);
		// Show the Up button in the action bar.
		//setupActionBar();
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	public void Waqood(View view) {
		
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://waqood.co/mcag"));
		startActivity(browserIntent);
	}
}
