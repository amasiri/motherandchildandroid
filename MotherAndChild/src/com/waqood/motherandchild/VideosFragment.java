package com.waqood.motherandchild;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStyle;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.waqood.motherandchild.adapters.VideosAdapter;
import com.waqood.motherchild.helpers.HttpCalls;
import com.waqood.motherchild.helpers.NiceMessage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class VideosFragment extends Fragment implements OnInitializedListener, 
com.google.android.youtube.player.YouTubeThumbnailView.OnInitializedListener,
OnItemClickListener{
//	private static final String PLAYLIST_ID = "ECAE6B03CA849AD332";
//	private static final String PLAYLIST_ID = "UCNLTKIilEhaReXy5ET-Sprg";
	
	private int count=0;
	
	private PullToRefreshListView list;
	private VideosAdapter adapter;
	ArrayList<HashMap<String, String>> listContent = new ArrayList<HashMap<String, String>>();
	private ArrayList<Drawable> thumbnailDrawableList = new ArrayList<Drawable>();
	private ArrayList<String> titleList = new ArrayList<String>();
	private ArrayList<String> videoIdsList = new ArrayList<String>();

	private static final int RECOVERY_DIALOG_REQUEST = 1;
	public View root;
//	public ImageView thumbnailImage;
	private YouTubeThumbnailLoader thumbnailLoader;
	private YouTubeThumbnailView thumbnailView;

	private AdView mAdView;

	private VideosFragment mClass;

	private RelativeLayout rl_main;
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.video_fragment_layout,
				container, false);

		rl_main = (RelativeLayout) rootView.findViewById(R.id.RL_MAIN);

		int bgColor = getActivity().getResources().getColor(R.color.green);
		int textColor = getActivity().getResources().getColor(R.color.white);

		NiceMessage.initNiceMessage(getActivity(), rl_main,  textColor, bgColor);

		mClass = this;
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);

		mAdView = (AdView) rootView.findViewById(R.id.adView);
		mAdView.loadAd(new AdRequest.Builder().build());

		list = (PullToRefreshListView) rootView.findViewById(R.id.LIST);
		adapter = new VideosAdapter(getActivity(), titleList,
				thumbnailDrawableList);
		list.setAdapter(adapter);

		list.setOnItemClickListener(this);

		thumbnailView = new YouTubeThumbnailView(getActivity());
		thumbnailView.initialize(DeveloperKey.DEVELOPER_KEY, this);
		
		list.setOnRefreshListener(new OnRefreshListener<ListView>() {
			@Override
			public void onRefresh(PullToRefreshBase<ListView> refreshView) {

				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						thumbnailView.initialize(DeveloperKey.DEVELOPER_KEY,
								mClass);
						count = 0;
						titleList.clear();
						thumbnailDrawableList.clear();
						videoIdsList.clear();
					}
				});

			}
		});

		root = rootView;

		return rootView;
	}
	



	@Override
	public void onInitializationSuccess(Provider provider,
			YouTubePlayer player, boolean wasRestored) {
		if (!wasRestored) {
			
		    player.setPlayerStyle(PlayerStyle.DEFAULT);

			
		}
	}
	 private void showBar() {
			if (getActivity() == null)
				return;
			MainScreens ra = (MainScreens) getActivity();
			ra.showBar();
	}
	 private void hideBar() {
			if (getActivity() == null)
				return;
			MainScreens ra = (MainScreens) getActivity();
			ra.hideBar();
	
	 }
	

	@Override
	public void onInitializationFailure(Provider arg0,
			YouTubeInitializationResult errorReason) {
		if (errorReason.isUserRecoverableError()) {
			errorReason.getErrorDialog(getActivity(), RECOVERY_DIALOG_REQUEST).show();
		} else {
			String errorMessage = String.format(
					"There was an error initializing the YouTubePlayer",
					errorReason.toString());
			Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
		}		
	}




	@Override
	public void onInitializationFailure(YouTubeThumbnailView arg0,
			YouTubeInitializationResult arg1) {


	     adapter.notifyDataSetChanged();
			list.onRefreshComplete();

	}




	@Override
	public void onInitializationSuccess(YouTubeThumbnailView thumbView,
			YouTubeThumbnailLoader thumbLoader) {

	    adapter.notifyDataSetChanged();
		list.onRefreshComplete();

		showBar();
		this.thumbnailLoader = thumbLoader;
		this.thumbnailView=thumbView;
		thumbLoader.setPlaylist(Constants.channelID);
		thumbLoader.setOnThumbnailLoadedListener(new ThumbnailListener());

		
	}

	  private final class ThumbnailListener implements
      YouTubeThumbnailLoader.OnThumbnailLoadedListener {

		  
    @Override
    public void onThumbnailLoaded(YouTubeThumbnailView thumbnail, String videoId) {

  
    	String title = getTitleQuietly(videoId);
    	titleList.add(title);
    	thumbnailDrawableList.add(thumbnail.getDrawable());
    	videoIdsList.add(videoId);
    	Log.d(this.getClass().getSimpleName(), " thumb LOADED  " + title + "counter " + count);


		
    	// change logic
		if(thumbnailLoader.hasNext())
		{
			count++;
			thumbnailLoader.next();
			//adapter.notifyDataSetChanged();
			
		}
		else
		{
			adapter.notifyDataSetChanged();
			hideBar();
		}
		
    }

    @Override
    public void onThumbnailError(YouTubeThumbnailView thumbnail,
        YouTubeThumbnailLoader.ErrorReason reason) {

	
			
			HttpCalls.handleFail(getActivity());
			hideBar();
			list.onRefreshComplete();
	        	
			hideBar();
    	

    }

  }

	  public static String getTitleQuietly(String id) {
		  String youtubeUrl= "http://www.youtube.com/watch?v=" +id ;
	        try {
	            if (youtubeUrl != null) {
	                URL embededURL = new URL("http://www.youtube.com/oembed?url=" + youtubeUrl + "&format=json");
	               
	                return new JSONObject(IOUtils.toString(embededURL)).getString("title");
	            }
	        } catch (JSONException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        return null;
	    }




	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
	
		position--;
		Intent n = new Intent(getActivity(),FullScreen.class);
		n.putExtra("id", videoIdsList.get(position));
		startActivity(n);

	}
	
	public void onBack()
	{

	}


	
	@Override
	public void onResume() {
        mAdView.resume();
    	
		 LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mMessageReceiver,
			      new IntentFilter(Constants.backPressedFilter));
		super.onResume();
	}
	
	@Override
	public void onPause() {
		  LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mMessageReceiver);
	        mAdView.pause();

		  super.onPause();
	}
	
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		  @Override
		  public void onReceive(Context context, Intent intent) {
		    // Extract data included in the Intent
		  //  String message = intent.getStringExtra("message");
		    //Log.d("receiver", "Got message: " + message);
			  onBack();
		  }
		};
		
		
	
	@Override
	public void onDestroy() {
		thumbnailLoader.release();
		mAdView.destroy();
		super.onDestroy();
	}

}
