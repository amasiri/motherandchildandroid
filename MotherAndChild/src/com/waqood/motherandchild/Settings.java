package com.waqood.motherandchild;

import java.util.ArrayList;
import com.waqood.motherandchild.adapters.SettingsAdapter;

import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.support.v4.app.NavUtils;

public class Settings extends Activity implements OnItemClickListener {
	private ListView list;
	private SettingsAdapter adapter;
	private ArrayList<String> titleList = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings_layout);
		// Show the Up button in the action bar.
		setupActionBar();
		
	  		list = (ListView) findViewById(R.id.LIST);
			adapter = new SettingsAdapter(this, titleList);
			list.setAdapter(adapter);	
			list.setOnItemClickListener(this);
			
			String[] settingsStrList = getResources().getStringArray(R.array.settings);
			

			for (int i = 0; i < settingsStrList.length; i++) {

		    titleList.add(settingsStrList[i]);	
		    
			}
			adapter.notifyDataSetChanged();

	}
	
	 @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	 {
		 if(requestCode ==100)
		 {
			 if (resultCode == 111 ) 
					if (getParent() == null) {
					    setResult(111);
					} else {
					    getParent().setResult(111);
					}
					finish();
					
		 }

		 
		 
		super.onActivityResult( requestCode,  resultCode,  data) ;	
	 }

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
				getActionBar().setHomeAsUpIndicator(R.drawable.ic_home_up);
			}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	//	getMenuInflater().inflate(R.menu.settings, menu);
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:

			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {

		if(position==0)
		 startActivityForResult(new Intent(this,Options.class), 100);
		else if(position==1)
		{
			Intent n = new Intent(this,WebViewActivity.class);
			n.putExtra("type", 1);

			startActivity(n);

		}
		else if(position==2)
		{
			Intent n = new Intent(this,WebViewActivity.class);
			n.putExtra("type", 2);

			startActivity(n);
		}
		else if(position==4)
		{
/*			//TODO remove this later
			Log.d(this.getClass().getSimpleName(), " current week " + SdCalls.getCurrentWeek(this));
			Log.d(this.getClass().getSimpleName(), " old week " + SdCalls.getOldWeek(this));
			Log.d(this.getClass().getSimpleName(), " is same ? " + SdCalls.isNewWeekYet(this));
			SdCalls.setOldWeek(this, 2);
			Log.d(this.getClass().getSimpleName(), " old week " + SdCalls.getOldWeek(this));
*/

			
			 startActivityForResult(new Intent(this,About.class), 200);
		}
		else if (position==3)
		{
			Intent emailIntent3 = new Intent(android.content.Intent.ACTION_SEND);

			String[] recipients3 = new String[] { "info@motherchildguide.com" };

			emailIntent3
					.putExtra(android.content.Intent.EXTRA_EMAIL, recipients3);
			emailIntent3.putExtra(android.content.Intent.EXTRA_TEXT,
					"");
			emailIntent3.setType("text/plain");
			startActivity(Intent.createChooser(emailIntent3, "Email us using..."));

		}

	}

}
