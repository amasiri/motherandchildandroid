package com.waqood.motherandchild;


import org.joda.time.DateTime;
import org.joda.time.Weeks;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.waqood.motherandchild.alarm.SampleAlarmReceiver;
import com.waqood.motherchild.helpers.HttpCalls;
import com.waqood.motherchild.push.PushRegisterService;


import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class Options extends Activity implements OnTouchListener, OnCheckedChangeListener, OnClickListener {
	 static final int DATE_DIALOG_ID = 999;

	 public static final String RESPOND = "respond";
		public static final String MESSAGE = "message";

	private EditText date_et;
	private int mYear;
	private int mMonth;
	private int mDay;
	public Options mClass;
    SampleAlarmReceiver alarm = new SampleAlarmReceiver();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setProgressBarIndeterminate(true);
		setContentView(R.layout.options_layout);
		// Show the Up button in the action bar.
		setupActionBar();
		
		     

		mClass = this;
		RelativeLayout rl_logout = (RelativeLayout) findViewById(R.id.RL_LOGOUT);
		rl_logout.setOnClickListener(this);
		
		
		Switch   switchButtonLocal = (Switch) findViewById(R.id.NOTI_SWITCH);

		switchButtonLocal.setChecked(SdCalls.getNotiLocal(this));
		switchButtonLocal.setOnCheckedChangeListener(this);
		
		Switch   switchButtonglobal = (Switch) findViewById(R.id.NOTI_ARTICLE_SWITCH);

		switchButtonglobal.setChecked(SdCalls.getNoti(this));
		switchButtonglobal.setOnCheckedChangeListener(this);
		
		   date_et = (EditText) findViewById(R.id.DATE_ET);
	        date_et.setOnTouchListener(this);
	  	        
	        mYear = SdCalls.getYear(this);
	        mMonth = SdCalls.getMonth(this);
	        mDay = SdCalls.getday(this);
	        date_et.setText(
	    	        new StringBuilder()
	    	                .append(mDay).append("/")
	    	                .append(mMonth ).append("/")
	    	                .append(mYear));

	        TextView useremail= (TextView) findViewById(R.id.USER_EMAIL);
	        
	        useremail.setText(SdCalls.getUseremail(this));
	
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
				getActionBar().setHomeAsUpIndicator(R.drawable.ic_home_up);
			}


	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	public boolean onTouch(View v, MotionEvent event) {
		showDialog(DATE_DIALOG_ID);
		return false;
	}
	@Override
	protected Dialog onCreateDialog(int id) {
	    switch (id) {
	    case DATE_DIALOG_ID:
	    	DatePickerDialog dp = new DatePickerDialog(this,
                    mDateSetListener,
                    mYear, mMonth, mDay);
	    	LayoutInflater inflater = getLayoutInflater();
	    	View view=inflater.inflate(R.layout.dialog_title, null);
	    	dp.setCustomTitle(view);
	    	
	    	return dp;
	    	
	    
	    }
	    return null;
	} 

	   // updates the date we display in the TextView
	private void updateDisplay() {
	    date_et.setText(
	        new StringBuilder()
	                .append(mDay).append("/")
	                .append(mMonth + 1).append("/")
	                .append(mYear));
		SdCalls.setDate(this, mYear, mMonth+1, mDay);

		updateDate();

	}

	private void updateDate( ) {
		
		String month = String.valueOf(mMonth+1);
		String date = mDay+"/"+month+"/"+mYear;

		showBar();
	//	String url = "username="+SdCalls.getUseremail(this).replace("@", ".") + "&date="+mDay+"/"+month+"/"+mYear;	
	//	Log.d(this.getClass().getSimpleName(), "URL " + Constants.DUE_DATE_URL + url);
		RequestParams httpParams = new RequestParams();
		httpParams.put("username", SdCalls.getUseremail(this).replace("@", "."));
		httpParams.put("date", date);
		httpParams.put("service", Constants.UPDATE_DUE_DATE_POST_PARAM);
		HttpCalls.post(Constants.DUE_DATE_POST_URL , httpParams, httpResponse);
			/*Log.d(this.getClass().getSimpleName(), "URL " + SdCalls.getUseremail(this).replace("@", ".")
		+ " __ " + mDay+"/"+month+"/"+mYear );
			
			Log.d(this.getClass().getSimpleName(), "username  " + date);
*/
	};
	
	
	JsonHttpResponseHandler httpResponse = new JsonHttpResponseHandler() {
		
	@Override
	public void onFailure(Throwable e, JSONObject str) {

		


		showBar();
		super.onFailure(e, str);
		e.printStackTrace();
	}
	
	public void onSuccess(JSONObject response) {
	
		hideBar();
		try {
			if(response.getInt(RESPOND) == 0)
			{
				//Toast.makeText(Options.this, response.getString(MESSAGE), Toast.LENGTH_SHORT).show();
			}
			else if(response.getInt(RESPOND) == 1)
			{

			}
		} catch (JSONException e) {

			e.printStackTrace();
		}
	};

};


	// the callback received when the user "sets" the date in the dialog
	private DatePickerDialog.OnDateSetListener mDateSetListener =
	        new DatePickerDialog.OnDateSetListener() {
	            public void onDateSet(DatePicker view, int year, 
	                                  int monthOfYear, int dayOfMonth) {
	                
	    			DateTime today = new DateTime();
	    			DateTime expected = new DateTime(year, monthOfYear+1, dayOfMonth, 00,00);

	    			int weeks = Weeks.weeksBetween(today, expected).getWeeks();

    				Log.d(this.getClass().getSimpleName(), "weeks " + weeks);

	    			if(weeks<(Constants.MAX_WEEKS) && weeks>0)
	    			{
	            	mYear = year;
	                mMonth = monthOfYear;
	                mDay = dayOfMonth;
	                updateDisplay();
	                SdCalls.setOldWeek(mClass, SdCalls.getCurrentWeek(mClass));
	                
	    			}
	    			else
	    			{
	    				String msg  = "تاريخ الولادة يجب ان يكون في غضون ٩ أشهر";
	    				Toast.makeText(mClass, msg, Toast.LENGTH_LONG).show();
	    			}
	         
	                 }
	        };

	@Override
	public void onCheckedChanged(CompoundButton button, boolean status) {

		button.setChecked(status);
		
		if(button.getId() == R.id.NOTI_ARTICLE_SWITCH)
		{
			SdCalls.setNoti(this, status);
			setupPush(status);
		}
		else
		{
		if(status)
		{
			startSchedule();
		}
		else
		{
			stopSchedule();
		}
		SdCalls.setNotiLocal(this, status);
		}
	}

	@Override
	public void onClick(View v) {


		SdCalls.setNoti(this, false);
		setupPush(false);
		 
		if(HttpCalls.isNetworkAvailable(this))
			SdCalls.reset(this);
		if (getParent() == null) {
		    setResult(111);
		} else {
		    getParent().setResult(111);
		}
		finish();

	}

	
	private void stopSchedule() {

		alarm.cancelAlarm(this);

	}
	private void startSchedule() {

        alarm.setAlarm(this);

		
	}
	
	private void setupPush(boolean status) {

		Intent i = new Intent(this, PushRegisterService.class);	 
		i.putExtra(Constants.PUSH_STATUS, status);

		Log.d(this.getClass().getSimpleName(), " setupPush " + status);
		startService(i);
	}
	
    public void showBar() {
	 	setProgressBarIndeterminateVisibility(true);




}
 public void hideBar() {
	 	setProgressBarIndeterminateVisibility(false);

}
}
