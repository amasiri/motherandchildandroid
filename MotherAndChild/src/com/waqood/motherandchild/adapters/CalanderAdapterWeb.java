package com.waqood.motherandchild.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import com.waqood.motherandchild.Constants;
import com.waqood.motherandchild.Details;
import com.waqood.motherandchild.R;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CalanderAdapterWeb extends BaseAdapter     {


	ArrayList<HashMap<String, String>> listContent = new ArrayList<HashMap<String, String>>();

	private Activity activity;
	private static LayoutInflater inflater = null;

	public CalanderAdapterWeb(Activity Theactivity,ArrayList<HashMap<String, String>> listContent) 
	{
		this.listContent = listContent;
		activity = Theactivity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {

		return listContent.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		
		if (convertView == null)
		{

			convertView = inflater.inflate(R.layout.calender_cell_web,parent, false);
			
		}
		
		WebView webv = (WebView) convertView.findViewById(R.id.ARTICLE_WEB);
		TextView weekTitle = (TextView) convertView.findViewById(R.id.WEEK_TITLE);
		ImageView image = (ImageView) convertView.findViewById(R.id.IMAGE_ARTICLE);

	//	cell_btn.setOnClickListener(this);
		HashMap<String, String> Data = new HashMap<String, String>();

		Data = listContent.get(position);
		String title = Data.get("title");
		int id = Integer.parseInt(Data.get("id"));

		weekTitle.setText(title);

	//	String imageName = "week_" + String.valueOf(position+Constants.WEEKS_OFFSET) ;//+ ".png";

	//	int id = activity.getResources().getIdentifier(imageName, "drawable", activity.getPackageName());
		image.setImageResource(id);

		
		String htmlFileTitle = "file:///android_asset/week_" + String.valueOf(position+Constants.WEEKS_OFFSET) + ".html";
	   
	      webv.loadUrl(htmlFileTitle); 
	        webv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
	        webv.setBackgroundColor(0x00000000);
	        if (Build.VERSION.SDK_INT >= 11) webv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
	        webv.setWebViewClient(new WebViewClient()
	        {
	            @Override
	            public void onPageFinished(WebView view, String url)
	            {
	            	view.setBackgroundColor(0x00000000);
	                if (Build.VERSION.SDK_INT >= 11) view.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
	                view.setAlpha(0.7f);
	            }
	        });
	  

	        
	        webv.setTag(convertView);
	        
	        webv.setOnTouchListener(new View.OnTouchListener() {
	            @Override
	            public boolean onTouch(View v, MotionEvent event) {
	                if (event.getAction() == MotionEvent.ACTION_UP) {

	                	Click(position);
	                }
	                return false;
	            }
	        });
	        
				return convertView;
	}



	public void Click(int position) {
		

		Intent n = new Intent(activity,Details.class);
		n.putExtra("title", listContent.get(position).get("title"));
		n.putExtra("index", position);

		activity.startActivity(n);		
	}

	
	public static Bitmap getCircularBitmap(Bitmap bitmap) {
	    Bitmap output;

	    if (bitmap.getWidth() > bitmap.getHeight()) {
	        output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Config.ARGB_8888);
	    } else {
	        output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Config.ARGB_8888);
	    }

	    Canvas canvas = new Canvas(output);

	    final int color = 0xff424242;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

	    float r = 0;

	    if (bitmap.getWidth() > bitmap.getHeight()) {
	        r = bitmap.getHeight() / 2;
	    } else {
	        r = bitmap.getWidth() / 2;
	    }

	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    canvas.drawCircle(r, r, r, paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(bitmap, rect, rect, paint);
	    return output;
	}

	}
