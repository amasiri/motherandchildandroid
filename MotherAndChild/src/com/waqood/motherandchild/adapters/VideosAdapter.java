package com.waqood.motherandchild.adapters;

import java.util.ArrayList;
import com.waqood.motherandchild.R;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class VideosAdapter extends BaseAdapter    {

	  public View root;
		public ImageView thumbnailImage; 
		private ArrayList<Drawable> thumbnailDrawableList = new ArrayList<Drawable>();
		private ArrayList<String> titleList = new ArrayList<String>();

		
	public static final String PREFS_NAME = "MyPrefsFile";

	
	private Activity activity;
	private static LayoutInflater inflater = null;

	public VideosAdapter(Activity Theactivity, ArrayList<String> titleList ,ArrayList<Drawable> thumbnailDrawableList) 
	{


		this.thumbnailDrawableList = thumbnailDrawableList;
		this.titleList = titleList;
		activity = Theactivity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public int getCount() {

		return thumbnailDrawableList.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		
		if (convertView == null)
		{

			convertView = inflater.inflate(R.layout.video_cell,parent, false);
			
		}
/*		  YouTubePlayerSupportFragment youTubePlayerFragment=(YouTubePlayerSupportFragment)  getFragmentManager().findFragmentById(R.id.youtube_fragment);
		  youTubePlayerFragment.initialize(DeveloperKey.DEVELOPER_KEY, activity);
*/

		 ArrayList<Drawable> thumbnailDrawableList = new ArrayList<Drawable>();
		 ArrayList<String> titleList = new ArrayList<String>();
		 thumbnailDrawableList = this.thumbnailDrawableList;
		 titleList = this.titleList;
		 Drawable thumbnailDrawable = thumbnailDrawableList.get(position);
		 String title = titleList.get(position);

//		 String message = Data.get("msg");

		TextView tv = (TextView) convertView.findViewById(R.id.VIDEO_TITLE);
		tv.setText(title);
		
		ImageView thumbnailImage = (ImageView) convertView.findViewById(R.id.THUMBNAIL);
		
		thumbnailImage.setImageDrawable(thumbnailDrawable);

		return convertView;
	}



	}
