package com.waqood.motherandchild.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;

import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.callback.AjaxStatus;

import com.waqood.motherandchild.R;
import com.waqood.motherchild.helpers.MyImageView;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ArticlesAdapter extends BaseAdapter    {


	ArrayList<HashMap<String, String>> listContent = new ArrayList<HashMap<String, String>>();

	private Activity activity;
	private static LayoutInflater inflater = null;
	private int mTextIndex = 200; //200
	public ArticlesAdapter(Activity Theactivity,ArrayList<HashMap<String, String>> listContent) 
	{


		this.listContent = listContent;
		activity = Theactivity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public int getCount() {

		return listContent.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

		
		if (convertView == null)
		{

			convertView = inflater.inflate(R.layout.articles_cell,parent, false);
			

    		holder = new ViewHolder();
            holder.titleH = (TextView) convertView.findViewById(R.id.ARTICLE_TITLE);
            holder.contentH = (TextView) convertView.findViewById(R.id.ARTICLE_TEXT);
            holder.dateH = (TextView) convertView.findViewById(R.id.DATE);
            holder.imageH = (MyImageView) convertView.findViewById(R.id.IMAGE_ARTICLE);

            convertView.setTag(holder);
  
        }
        else
        {
            holder = (ViewHolder) convertView.getTag();
        }

		
			
		HashMap<String, String> Data = new HashMap<String, String>();
		Data = listContent.get(position);
		String titleStr = Data.get("title");
		String dateStr = Data.get("date");
		String contentStr = Data.get("content");
		String imageStr = Data.get("image");

		
		AQuery aq = new AQuery(convertView);
		
		aq.id(holder.imageH).image(imageStr, true, true, 0, 0, new BitmapAjaxCallback(){

	        @Override
	        public void callback(String url, ImageView iv, Bitmap bm, AjaxStatus status){
	                               
	        	
	                iv.setImageBitmap(getCircularBitmap(bm));
	                
	        }
	        
	});
	

		holder.contentH.setText(contentStr + "... " );

		
		try{
		String articleStr = contentStr;
		articleStr = (String) articleStr.subSequence(0, mTextIndex);
		holder.contentH.setText(html2text(contentStr));
		holder.contentH.append(Html.fromHtml(activity.getString(R.string.more_text)));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		holder.titleH.setText(titleStr);
		holder.dateH.setText(dateStr.subSequence(0, dateStr.indexOf("T")));
		

	       

		return convertView;
	}
	public static String html2text(String html) {
	    return Jsoup.parse(html).text();
	}

	static class ViewHolder {
        TextView titleH;
        TextView contentH;
        TextView dateH;
        TextView idH;
        MyImageView imageH;


    }
	
	public static Bitmap getCircularBitmap(Bitmap bitmap) {
	    Bitmap output;

	    if (bitmap.getWidth() > bitmap.getHeight()) {
	        output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Config.ARGB_8888);
	    } else {
	        output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Config.ARGB_8888);
	    }

	    Canvas canvas = new Canvas(output);

	    final int color = 0xff424242;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

	    float r = 0;

	    if (bitmap.getWidth() > bitmap.getHeight()) {
	        r = bitmap.getHeight() / 2;
	    } else {
	        r = bitmap.getWidth() / 2;
	    }

	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    canvas.drawCircle(r, r, r, paint);
	    paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	    canvas.drawBitmap(bitmap, rect, rect, paint);
	    return output;
	}

	}
