package com.waqood.motherandchild.adapters;

import java.util.ArrayList;

import com.waqood.motherandchild.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SettingsAdapter extends BaseAdapter    {

		private ArrayList<String> titleList = new ArrayList<String>();

		
	public static final String PREFS_NAME = "MyPrefsFile";

	
	private Activity activity;
	private static LayoutInflater inflater = null;

	public SettingsAdapter(Activity Theactivity, ArrayList<String> titleList )
	{
		this.titleList = titleList;
		activity = Theactivity;
		inflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public int getCount() {

		return titleList.size();
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		
		if (convertView == null)
		{

			convertView = inflater.inflate(R.layout.settings_cell,parent, false);
			
		}
		 String title = titleList.get(position);

		TextView tv = (TextView) convertView.findViewById(R.id.SETTINGS_TITLE);
		tv.setText(title);
		
		ImageView icon = (ImageView) convertView.findViewById(R.id.SETTINGS_ICON);
		
		switch (position) {
		case 0:
			icon.setImageResource(R.drawable.setting_ic);
			break;
		case 1:
			icon.setImageResource(R.drawable.calculator_ic);
			break;
		case 2:
			icon.setImageResource(R.drawable.calculator_ic);
			break;
		case 3:
			icon.setImageResource(R.drawable.contactus_ic);
			break;
		case 4:
			icon.setImageResource(R.drawable.info_ic);
			break;


		}
	
		return convertView;
	}



	}
