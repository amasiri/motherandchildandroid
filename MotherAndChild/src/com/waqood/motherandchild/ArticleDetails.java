package com.waqood.motherandchild;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

public class ArticleDetails extends Activity implements OnTouchListener {

	protected ImageLoader imageLoader = ImageLoader.getInstance();

	private TextView webv;
	private DisplayImageOptions optionsDisplay;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.article_details_layout_text_scroll);
		
			TextView title = (TextView) findViewById(R.id.DETAILS_TITLE);

		ImageView image = (ImageView) findViewById(R.id.DETAILS_IMAGE);

		Intent intent = getIntent();    
		String titleStr = intent.getExtras().getString("title");
		String content = intent.getExtras().getString("content");
		String imageUrl = intent.getExtras().getString("image");
	
		ImageOptions options = new ImageOptions();
		options.animation = AQuery.FADE_IN;
		options.memCache = true;
		options.fileCache =true;
		options.fallback=android.graphics.Color.TRANSPARENT;


		optionsDisplay = new DisplayImageOptions.Builder()
				.resetViewBeforeLoading(false).cacheInMemory(true)
				.showImageOnLoading(android.graphics.Color.TRANSPARENT)
				.cacheOnDisc(true)
				.imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
				.displayer(new FadeInBitmapDisplayer(300)).build();
		imageLoader.displayImage(imageUrl, image, optionsDisplay);
			

	        title.setText(titleStr);
	        
	      //  String html = "<html><body>Hello, World!</body></html>";
	        String mime = "text/html";
	        String encoding = "utf-8";

	        webv = (TextView)findViewById(R.id.DETAILS_WEB);
	        webv.setText(Html.fromHtml(content));
	      /*  webv = (WebView)findViewById(R.id.DETAILS_WEB);

	    //   String htmlContent = "<html lang=\"he\"><body><p dir=\"rtl\">" + content + "</p></body></html>";

	        Log.d(this.getClass().getSimpleName(), "CONTENT " + content);
	        webv.getSettings().setJavaScriptEnabled(true);
	        webv.loadDataWithBaseURL(null, content, mime, encoding, null);
	     	webv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
	        webv.setBackgroundColor(0x00000000);
	        if (Build.VERSION.SDK_INT >= 11) webv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
	        webv.setWebViewClient(new WebViewClient()
	        {
	            @Override
	            public void onPageFinished(WebView view, String url)
	            {
	            	webv.setBackgroundColor(0x00000000);
	                if (Build.VERSION.SDK_INT >= 11) webv.setLayerType(WebView.LAYER_TYPE_HARDWARE, null);
	            }
	        });
*/	setupActionBar();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.details, menu);
		return false;
	}
	
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
				getActionBar().setHomeAsUpIndicator(R.drawable.ic_home_up);
			}

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
//			webv.invalidate();
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {

	//	webv.invalidate();
		return false;
	}

}
