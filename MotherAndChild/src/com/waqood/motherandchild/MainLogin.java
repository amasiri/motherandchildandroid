package com.waqood.motherandchild;


import java.util.Locale;

import com.waqood.motherchild.helpers.HttpCalls;




import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.view.Menu;
import android.view.View;

public class MainLogin extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_login_layout);
		
		Locale locale = new Locale("ar");
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getApplicationContext().getResources()
				.updateConfiguration(config, null);

		if(SdCalls.isFirst(this)==false)
		{
			Intent n = new Intent(this,MainScreens.class);
			startActivity(n);
			finish();				
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return false;
	}

	public void registerClicked(View view) {
		if(HttpCalls.isNetworkAvailable(this))
		{
		Intent n = new Intent(this,Register.class);
		startActivity(n);
		}
		else
		{
			showDialogMessage();

		}
	}

	public void loginClicked(View view) {
		
		if (HttpCalls.isNetworkAvailable(this)) {

			Intent n = new Intent(this, Login.class);
			startActivity(n);
		} else {

			showDialogMessage();
			
		}
	}
	
	
	public  void showDialogMessage() {
		try{
		new AlertDialog.Builder(MainLogin.this)
				.setTitle("تنبيه")
				.setMessage(Constants.NO_CONNECTION)
				.setPositiveButton("اخفاء",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {

							}
						}).show();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
