package com.waqood.motherandchild;

public class Constants {

	/**
	 * Name of SD file
	 */
	public static final int TabsCount = 3;
	public static final String backPressedFilter = "backPressed";
	public static final int MAX_WEEKS = 42;
	public static final int WEEKS_OFFSET = 3;
	public static final String channelID = "UUGSgyfOtyv_2iIbk3t2s2HQ";

	public static final String BASE_URL = 	"http://motherchildguide.com/mobileapi/";
	public static final String RESET_PASSWORD_URL = "lostpwd/";
	public static final String TOKEN_URL = 	"savetoken/";
	public static final String LOGIN_URL = 	"login/?";
	public static final String SIGNUP_URL = 	"signup/";
	public static final String UPDATE_PROFILE_URL = 	"update/";
	public static final String UPDATE_TOKEN_URL = 	"token/";
	public static final String UPDATE_CHANNEL_URL = 	"channels_subscribe/";
	public static final String DUE_DATE_POST_URL = 	"custom_service/";
	public static final String UPDATE_DUE_DATE_POST_PARAM = 	"updatedeliverydate";
	public static final String GET_DUE_DATE_POST_PARAM = 	"getdeliverydate";

	public static final String PUSH_STATUS = 	"pushStatus";
	public static final String PUSH_SENDER_ID = 	"499895578571";

	public static final String NO_CONNECTION = 	"لا يوجد اتصال بالانترنت";
	public static final String CONNECTION_FAILURE = 	"حدث خطأ ما, الرجاء التحديث عن طريق سحب الشاشة";

    
}

