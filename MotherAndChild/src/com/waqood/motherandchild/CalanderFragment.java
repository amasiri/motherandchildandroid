package com.waqood.motherandchild;

import java.util.ArrayList;
import java.util.HashMap;

import com.fourmob.poppyview.PoppyViewHelper;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.waqood.motherandchild.adapters.CalanderAdapterWeb;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class CalanderFragment extends Fragment implements OnItemClickListener{
	private ListView list;
	private CalanderAdapterWeb adapter;
	ArrayList<HashMap<String, String>> listContent = new ArrayList<HashMap<String, String>>();
	private AdView mAdView;

	private PoppyViewHelper mPoppyViewHelper;

  	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View rootView = inflater.inflate(R.layout.calender_fragment_layout,
				container, false);	
		
		
	    mAdView = (AdView) rootView.findViewById(R.id.adView);
        mAdView.loadAd(new AdRequest.Builder().build());
	
        mPoppyViewHelper = new PoppyViewHelper(getActivity(), rootView);
		View poppyView = mPoppyViewHelper.createPoppyViewOnListView(R.id.LIST, R.layout.poppyview, new AbsListView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
			}
		});

		poppyView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if(SdCalls.getCurrentWeek(getActivity()) >0)
					list.setSelection((SdCalls.getCurrentWeek(getActivity())-Constants.WEEKS_OFFSET)-Constants.WEEKS_OFFSET+1);

				else
				{
					String msg  = "لم تصلي الى الأسبوع الرابع بعد";
					Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
			
				}
				Log.d(this.getClass().getSimpleName(), " WEEKS " + SdCalls.getCurrentWeek(getActivity()));
				Log.d(this.getClass().getSimpleName(), "MODIFY WEEKS " + (SdCalls.getCurrentWeek(getActivity())-Constants.WEEKS_OFFSET));

			}
		});

		listContent.clear();
		list = (ListView) rootView.findViewById(R.id.LIST);
		adapter = new CalanderAdapterWeb(getActivity(), listContent);
		list.setAdapter(adapter);	

		list.setOnItemClickListener(this);

	String[] articles = getResources().getStringArray(R.array.calander);
		
		HashMap<String, String> map = new HashMap<String, String>();


		
		for (int i = 0; i < articles.length; i++) {
			
			String imageName = "week_" + String.valueOf(i+Constants.WEEKS_OFFSET) ;//+ ".png";
			int id = getActivity().getResources().getIdentifier(imageName, "drawable",getActivity().getPackageName());
	    	map.put("id", String.valueOf(id));
	    	map.put("title", articles[i]);
	    	listContent.add(map);
			map = new HashMap<String, String>();

		}
		       
		adapter.notifyDataSetChanged();	 
		list.setSelection((SdCalls.getCurrentWeek(getActivity())-Constants.WEEKS_OFFSET)-Constants.WEEKS_OFFSET+1);


		return rootView;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		Intent n = new Intent(getActivity(),Details.class);
		n.putExtra("title", listContent.get(position).get("title"));
		n.putExtra("index", position);

		startActivity(n);

		
	}
	
	  @Override
	    public void onPause() {
	        mAdView.pause();
	        super.onPause();
	    }

	    @Override
	    public void onResume() {
	        super.onResume();
	        mAdView.resume();
	    }

	    @Override
		public void onDestroy() {
	        mAdView.destroy();
	        super.onDestroy();
	    }


}
